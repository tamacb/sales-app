import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sales/app/utils/app_colors.dart';

import 'app/routes/app_pages.dart';

void main() async {
  await GetStorage.init();
  runApp(
    // DevicePreview(
    //   enabled: !kReleaseMode,
    //   builder: (context) => GetMaterialApp(
    //       locale: DevicePreview.locale(context),
    //       builder: DevicePreview.appBuilder,
    //       title: "Sales App",
    //       initialRoute: AppPages.initialPage,
    //       getPages: AppPages.routes,
    //       theme: Themes.light,
    //       darkTheme: Themes.dark,
    //       themeMode: ThemeService().theme), // Wrap your app
    // ),
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: "Sales App",
        initialRoute: AppPages.initialPage,
        getPages: AppPages.routes,
        theme: Themes.light,
        darkTheme: Themes.dark,
        themeMode: ThemeService().theme);
  }
}

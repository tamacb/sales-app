import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginTextFormFieldBase extends StatelessWidget {
  const LoginTextFormFieldBase(
      {Key? key,
      required this.textEditingController,
      required this.controller,
      required this.obscureText,
      this.hintText,
      required this.passwordVisibility,
      this.suffix,
      this.textInputFormatter,
      this.returnValidation,
      this.logic,
      this.validator})
      : super(key: key);

  final TextEditingController textEditingController;
  final GetxController controller;
  final bool obscureText;
  final String? hintText;
  final bool passwordVisibility;
  final Widget? suffix;
  final List<TextInputFormatter>? textInputFormatter;
  final dynamic returnValidation;
  final String? logic;
  final Function? validator;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: key,
      validator: returnValidation,
      inputFormatters: textInputFormatter,
      controller: textEditingController,
      obscureText: obscureText,
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        labelStyle: GoogleFonts.nunitoSans(
          color: Colors.black,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
        hintText: hintText,
        hintStyle: GoogleFonts.nunitoSans(
          color: Colors.grey,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: BorderRadius.circular(50),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: BorderRadius.circular(50),
        ),
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.fromLTRB(30, 10, 20, 24),
        suffixIcon: suffix,
      ),
      style: GoogleFonts.nunitoSans(
        color: Colors.black,
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}

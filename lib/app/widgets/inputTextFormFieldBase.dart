import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class InputTextFormFieldBase extends StatelessWidget {
  const InputTextFormFieldBase(
      {Key? key,
      required this.textEditingController,
      required this.controller,
      required this.obscureText,
      this.hintText,
      this.label,
      required this.passwordVisibility,
      this.suffix,
      this.textInputFormatter,
      this.returnValidation,
      this.logic,
      this.validator})
      : super(key: key);

  final TextEditingController textEditingController;
  final GetxController controller;
  final bool obscureText;
  final String? hintText;
  final bool passwordVisibility;
  final Widget? suffix;
  final Widget? label;
  final List<TextInputFormatter>? textInputFormatter;
  final dynamic returnValidation;
  final String? logic;
  final Function? validator;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: Colors.black,
      key: key,
      validator: returnValidation,
      inputFormatters: textInputFormatter,
      controller: textEditingController,
      obscureText: obscureText,
      decoration: InputDecoration(
        label: label,
        labelStyle: GoogleFonts.nunitoSans(
          color: Colors.black,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
        hintText: hintText,
        hintStyle: GoogleFonts.nunitoSans(
          color: Colors.grey,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ), focusedBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.black),
      ),));
  }
}

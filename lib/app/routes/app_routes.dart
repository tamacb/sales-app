part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const ADD_DATA = _Paths.ADD_DATA;
  static const COBA = _Paths.COBA;
}

abstract class _Paths {
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const ADD_DATA = '/add-data';
  static const COBA = '/coba';
}

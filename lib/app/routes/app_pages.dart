import 'package:get/get.dart';

import 'package:sales/app/modules/add_data/bindings/add_data_binding.dart';
import 'package:sales/app/modules/add_data/views/add_data_view.dart';
import 'package:sales/app/modules/coba/bindings/coba_binding.dart';
import 'package:sales/app/modules/coba/views/coba_view.dart';
import 'package:sales/app/modules/home/bindings/home_binding.dart';
import 'package:sales/app/modules/home/views/home_view.dart';
import 'package:sales/app/modules/login/bindings/login_binding.dart';
import 'package:sales/app/modules/login/views/login_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const initialPage = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.ADD_DATA,
      page: () => AddDataView(),
      binding: AddDataBinding(),
    ),
    GetPage(
      name: _Paths.COBA,
      page: () => CobaView(),
      binding: CobaBinding(),
    ),
  ];
}

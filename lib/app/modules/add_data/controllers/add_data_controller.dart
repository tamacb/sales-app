import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sales/app/utils/common_utils.dart';

class AddDataController extends FullLifeCycleController {
  //TODO: Implement AddDataController
  final emailEditingController = TextEditingController();
  final passwordEditingController = TextEditingController();

  final RoundedLoadingButtonController btnControllerAddData = RoundedLoadingButtonController();

  final count = 0.obs;

  final _filePhotoLocation = ''.obs;
  final _namePhoto = 'Choose your CV'.obs;

  final listPhotoUpload = [].obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  Future getFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    );
    if (result != null) {
      File file = File(result.files.single.path.toString());
      PlatformFile fileName = result.files.last;
      logger.i(fileName.name);
      _filePhotoLocation.value = result.files.single.path.toString();
      _namePhoto.value = fileName.name;
      logger.wtf(file);
    } else {
      Get.snackbar('file not found', 'choose your image file');
    }
  }
}

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sales/app/routes/app_pages.dart';
import 'package:sales/app/widgets/inputTextFormFieldBase.dart';

import '../controllers/add_data_controller.dart';

class AddDataView extends GetView<AddDataController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: context.theme.bottomAppBarColor,
        leading: IconButton(
          icon: const Icon(Icons.keyboard_backspace, color: Colors.black,),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            color: context.theme.colorScheme.onPrimary),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Text('Tambah Data', style: GoogleFonts.nunitoSans(
                  color: Colors.grey,
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 5, top: 5),
                child: InputTextFormFieldBase(
                  label: Text('email', style: GoogleFonts.nunitoSans(
                    color: Colors.grey,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                  controller: controller,
                  hintText: 'email',
                  obscureText: false,
                  passwordVisibility: false,
                  textEditingController: controller.emailEditingController,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 5, top: 5),
                child: InputTextFormFieldBase(
                  label: Text('nama pelanggan', style: GoogleFonts.nunitoSans(
                    color: Colors.grey,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                  controller: controller,
                  hintText: 'nama pelanggan',
                  obscureText: false,
                  passwordVisibility: false,
                  textEditingController: controller.emailEditingController,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 5, top: 5),
                child: InputTextFormFieldBase(
                  label: Text('Telp', style: GoogleFonts.nunitoSans(
                    color: Colors.grey,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                  controller: controller,
                  hintText: 'telp',
                  obscureText: false,
                  passwordVisibility: false,
                  textEditingController: controller.emailEditingController,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 5, top: 5),
                child: InputTextFormFieldBase(
                  label: Text('Email', style: GoogleFonts.nunitoSans(
                    color: Colors.grey,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                  controller: controller,
                  hintText: 'Email',
                  obscureText: false,
                  passwordVisibility: false,
                  textEditingController: controller.emailEditingController,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 5, top: 5),
                child: InputTextFormFieldBase(
                  label: Text('jenis kelamin', style: GoogleFonts.nunitoSans(
                    color: Colors.grey,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                  controller: controller,
                  hintText: 'jenis kelamin',
                  obscureText: false,
                  passwordVisibility: false,
                  textEditingController: controller.emailEditingController,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 5, top: 5),
                child: InputTextFormFieldBase(
                  label: Text('usia', style: GoogleFonts.nunitoSans(
                    color: Colors.grey,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                  controller: controller,
                  hintText: 'jenis kelamin',
                  obscureText: false,
                  passwordVisibility: false,
                  textEditingController: controller.emailEditingController,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 5, top: 5),
                child: InputTextFormFieldBase(
                  label: Text('jabatan', style: GoogleFonts.nunitoSans(
                    color: Colors.grey,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                  controller: controller,
                  hintText: 'jabatan',
                  obscureText: false,
                  passwordVisibility: false,
                  textEditingController: controller.emailEditingController,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: (){
                      controller.getFile();
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 5, top: 5),
                      child: DottedBorder(
                        color: Colors.black,
                        strokeWidth: 1,
                        child: const Icon(Icons.add_photo_alternate, size: 60.0,),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                    child: Text('Upload Foto', style: GoogleFonts.nunitoSans(
                      color: Colors.grey,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: RoundedLoadingButton(
                    height: 35.0,
                    key: const Key('buttonLogin'),
                    valueColor: HexColor('#CC9933'),
                    color: Colors.black,
                    controller: controller.btnControllerAddData,
                    onPressed: () async {
                      Get.toNamed(Routes.HOME);
                      controller.btnControllerAddData.reset();
                    },
                    child: Text(
                      'Tambahkan',
                      style: TextStyle(color: HexColor('#CC9933')),
                    )),
              ),
            ],
          ),
        ),
      ),
      // floatingActionButton: FloatingActionButton.small(onPressed: () {
      //   Get.changeTheme(Get.isDarkMode ? ThemeData.light() : ThemeData.dark());
      // }),
    );
  }
}

import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sales/app/utils/common_utils.dart';
import 'package:sales/app/utils/constaUtils.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

class HomeController extends GetxController with WidgetsBindingObserver {
  //TODO: Implement HomeController

  final isHours = true;

  final count = 0.obs;
  final startedState = false.obs;
  final emailEditingController = TextEditingController();
  final passwordEditingController = TextEditingController();

  final RoundedLoadingButtonController btnControllerLogin = RoundedLoadingButtonController();

  final _filePhotoProfileLocation = ''.obs;
  final _namePhotoProfile = 'Choose your CV'.obs;

  final GetStorage _box = GetStorage();

  final StopWatchTimer stopWatchTimer = StopWatchTimer(
    mode: StopWatchMode.countUp,
    // onChange: (value) => print('onChange $value'),
    // onChangeRawSecond: (value) => logger.i('onChangeRawSecond $value'),
    // onChangeRawMinute: (value) => print('onChangeRawMinute $value'),
  );

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (_box.read(timerStartWork) != null) {
      if (state == AppLifecycleState.paused) {
        logger.i('lagi on paused');
        _box
            .write(workDuration, stopWatchTimer.secondTime.value)
            .whenComplete(() => logger.i('on paused ${_box.read(workDuration)}'));
        _box
            .write(timerPauseWork, DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()))
            .whenComplete(() => logger.i('on paused ${_box.read(timerPauseWork)}'));
      }
    }
    if (state == AppLifecycleState.resumed) {
      if (_box.read(workDuration) != null) {
        startedState.value = true;
        sumWorkingResume();
      }
    }
  }

  @override
  void onInit() {
    super.onInit();
    WidgetsBinding.instance!.addObserver(this);
    checkStartWork();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    WidgetsBinding.instance!.removeObserver(this);
  }

  sumWorkingResume() {
    var onWorkDuration = _box.read(workDuration);
    if (onWorkDuration != null) {
      try {
        var pausedTimer = _box.read(timerPauseWork);
        DateTime dateTimePaused = DateTime.parse(pausedTimer);
        DateTime dateTimeNow = DateTime.parse(DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()));

        Duration workingHours = dateTimeNow.difference(dateTimePaused);
        var totalWorkingTime = onWorkDuration + workingHours.inSeconds;
        logger.i('total working timer $totalWorkingTime');
        stopWatchTimer.onExecute.add(StopWatchExecute.reset);
        stopWatchTimer.clearPresetTime();
        stopWatchTimer.setPresetSecondTime(totalWorkingTime);
        stopWatchTimer.onExecute.add(StopWatchExecute.start);
      } catch (e) {
        logger.i(e);
      } finally {}
    }
  }

  Future getFilePhotoProfile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    );
    if (result != null) {
      File file = File(result.files.single.path.toString());
      PlatformFile fileName = result.files.last;
      logger.i(fileName.name);
      _filePhotoProfileLocation.value = result.files.single.path.toString();
      _namePhotoProfile.value = fileName.name;
      logger.wtf(file);
    } else {
      Get.snackbar('file not found', 'choose image profile');
    }
  }

  void stopTimerWork() {
    _box.write(workDuration, null);
    _box.write(timerStartWork, null);
    _box.write(timerEndWork, DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()));
    startedState.value = false;
    stopWatchTimer.onExecute.add(StopWatchExecute.reset);
    stopWatchTimer.clearPresetTime();
  }

  void pauseTimerWork() {
    stopWatchTimer.onExecute.add(StopWatchExecute.reset);
  }

  void startTimerWork() {
    stopWatchTimer.onExecute.add(StopWatchExecute.start);
    startedState.value = true;
    _box
        .write(timerStartWork, DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()))
        .whenComplete(() => logger.i('started ${_box.read(timerStartWork)}'));
  }

  void checkStartWork() {
    if (_box.read(workDuration) != null) {
      startedState.value = true;
      sumWorkingResume();
    }
  }
}

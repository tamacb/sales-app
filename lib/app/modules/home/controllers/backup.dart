import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sales/app/utils/common_utils.dart';
import 'package:sales/app/utils/constaUtils.dart';

class HomeController extends GetxController with WidgetsBindingObserver {
  //TODO: Implement HomeController

  final DateTime now = DateTime.now();

  final count = 0.obs;
  final startedState = false.obs;
  final emailEditingController = TextEditingController();
  final passwordEditingController = TextEditingController();

  final RoundedLoadingButtonController btnControllerLogin = RoundedLoadingButtonController();

  Timer _timer = Timer(1.seconds, () {});

  final _filePhotoProfileLocation = ''.obs;
  final _namePhotoProfile = 'Choose your CV'.obs;

  final GetStorage _box = GetStorage();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      _box.write(workDuration, count.value).whenComplete(() => logger.i('total duration ${_box.read(workDuration)}'));
    }
    if (state == AppLifecycleState.resumed) {
      if (_box.read(timerStartWork) != null) getDifferenceTime();
    }
    if (state == AppLifecycleState.paused) {
      if (_box.read(timerStartWork) != null) {
        _box
            .write(timerPauseWork, DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()))
            .whenComplete(() => logger.i('paused work ${_box.read(timerPauseWork)}'));
      }
      // if (_box.read(timerStartWork) != null) getDifferenceTime();
    }
  }

  @override
  void onInit() {
    super.onInit();
    WidgetsBinding.instance!.addObserver(this);
    logger.i(_box.read(workDuration));
    checkStartedStateWork();
  }

  void checkStartedStateWork() {
    if (_box.read(workDuration) == null || _box.read(workDuration) == 0) {
      startedState.value = false;
    } else {
      startedState.value = true;
    }
  }

  @override
  void onReady() {
    super.onReady();
    if (_box.read(timerStartWork) != null) getDifferenceTime();
  }

  @override
  void onClose() {
    super.onClose();
    WidgetsBinding.instance!.removeObserver(this);
    _box.write(workDuration, count.value).whenComplete(() => logger.i(_box.read(workDuration)));
  }

  void increment() => count.value++;

  String toTimerDuration() {
    return Duration(seconds: count.value).formatTimeWork();
  }

  Future<void> timerStart() async {
    _timer = Timer.periodic(1.seconds, (Timer t) {
      count.value = t.tick;
    });
    startedState.toggle();
    _box
        .write(timerStartWork, DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()))
        .whenComplete(() => logger.i('started work ${_box.read(timerStartWork)}'));
  }

  Future<void> timerStop() async {
    _timer.cancel();
    startedState.toggle();
    _box
        .write(timerEndWork, DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()))
        .whenComplete(() => logger.i('ended work ${_box.read(timerEndWork)}'));
    _box.write(workDuration, null).whenComplete(() => logger.i('durasi stop di ${_box.read(workDuration)}'));
    _box.write(timerPauseWork, null).whenComplete(() => logger.i('time pause deleted ${_box.read(timerPauseWork)}'));
    _box.write(timerStartWork, null).whenComplete(() => logger.i('time start deleted ${_box.read(timerStartWork)}'));
    _box.write(timerEndWork, null).whenComplete(() => logger.i('time ende deleted ${_box.read(timerEndWork)}'));
    count.value = 0;
  }

  void getDifferenceTime() {
    // var startTimer = _box.read(timerStartWork);
    // var endTimer = _box.read(timerEndWork);
    var pausedTimer = _box.read(timerPauseWork);
    // DateTime dateTimeStart = DateTime.parse(startTimer);
    // DateTime dateTimeEnd = DateTime.parse(endTimer ?? pausedTimer);
    DateTime dateTimePaused = DateTime.parse(pausedTimer);
    DateTime dateTimeNow = DateTime.parse(DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()));
    Duration workingHours = dateTimeNow.difference(dateTimePaused);
    // logger.i('total differences $workingHours');
    format(Duration d) => d.toString().split('.').first.padLeft(8, "0");
    // logger.i('start $startTimer');
    logger.i('paused format $pausedTimer}');
    logger.i('ini total format ${format(workingHours)}');
    count.value = count.value + workingHours.inSeconds;
    logger.i('in second nihh ${count.value = workingHours.inSeconds}');
    if (_box.read(workDuration) != null){

    }
  }

  Future getFilePhotoProfile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    );
    if (result != null) {
      File file = File(result.files.single.path.toString());
      PlatformFile fileName = result.files.last;
      logger.i(fileName.name);
      _filePhotoProfileLocation.value = result.files.single.path.toString();
      _namePhotoProfile.value = fileName.name;
      logger.wtf(file);
    } else {
      Get.snackbar('file not found', 'choose image profile');
    }
  }
}

extension on Duration {
  String formatTimeWork() => '$this'.split('.')[0].padLeft(8, '0');
}

import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:sales/app/modules/home/controllers/home_controller.dart';
import 'package:sales/app/routes/app_pages.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class HomeView extends GetView<HomeController> {
  List<SalesData> data = [
    SalesData('Samsung Fold', 35),
    SalesData('Samsung GYoung', 28),
    SalesData('Samsung j2 pr', 100),
    SalesData('Samsung note 1', 32),
  ];

  List<SalesData> dataLines = [
    SalesData('Samsung Fold', 135),
    SalesData('Samsung GYoung', 228),
    SalesData('Samsung j2 pr', 134),
    SalesData('Samsung note 1', 232),
    SalesData('May', 140),
    SalesData('Samsung a', 235),
    SalesData('Samsung v', 128),
    SalesData('Samsung b', 234),
    SalesData('Samsung x', 232),
    SalesData('b', 240),
    SalesData('Samsung q', 235),
    SalesData('Samsung w', 128),
    SalesData('Samsung e', 334),
    SalesData('Samsung ev 1', 432),
    SalesData('bb', 340)
  ];

  List<SalesData> dataLines1 = [
    SalesData('Samsung Fold', 1),
    SalesData('Samsung GYoung', 2),
    SalesData('Samsung j2 pr', 3),
    SalesData('Samsung note 1', 4),
    SalesData('May', 5),
    SalesData('Samsung a', 11),
    SalesData('Samsung v', 12),
    SalesData('Samsung b', 13),
    SalesData('Samsung x', 14),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(Get.height * 0.25),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        const SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              Get.toNamed(Routes.ADD_DATA);
                            },
                            child: Container(
                              height: 30.0,
                              width: Get.width * 0.15,
                              child: Center(
                                  child: Text(
                                'Tambah Data',
                                style: GoogleFonts.nunitoSans(
                                  color: Colors.blueAccent,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )),
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(50.0), color: Colors.white),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20.0,
                        ),
                        Expanded(
                          flex: 1,
                          child: Obx(() => Container(
                                height: 30.0,
                                width: Get.width * 0.17,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: StreamBuilder<int>(
                                          stream: controller.stopWatchTimer.rawTime,
                                          initialData: controller.stopWatchTimer.rawTime.value,
                                          builder: (context, snap) {
                                            final value = snap.data!;
                                            final displayTime = StopWatchTimer.getDisplayTime(value,
                                                hours: controller.isHours, milliSecond: false);
                                            return Center(
                                              child: Text(
                                                displayTime,
                                                style: GoogleFonts.nunitoSans(
                                                  color: Colors.blueAccent,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            );
                                          },
                                        )),
                                    Expanded(
                                      flex: 1,
                                      child: controller.startedState.value == true
                                          ? GestureDetector(
                                              onTap: () {
                                                Get.defaultDialog(
                                                    title: 'Stop Kerja',
                                                    content: const Text('ambil gajimu, lalu liburan'),
                                                    onConfirm: () {
                                                      controller.stopTimerWork();
                                                      Get.until((route) => Get.isDialogOpen == false);
                                                    },
                                                    onCancel: () {});
                                              },
                                              child: Container(
                                                child: Center(
                                                    child: Text(
                                                  'stop',
                                                  style: GoogleFonts.nunitoSans(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                )),
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(50.0), color: Colors.red),
                                              ),
                                            )
                                          : GestureDetector(
                                              onTap: () {
                                                controller.startTimerWork();
                                              },
                                              child: Container(
                                                child: Center(
                                                    child: Text(
                                                  'start',
                                                  style: GoogleFonts.nunitoSans(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                )),
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(50.0), color: Colors.green),
                                              ),
                                            ),
                                    ),
                                  ],
                                ),
                                decoration:
                                    BoxDecoration(borderRadius: BorderRadius.circular(50.0), color: Colors.white),
                              )),
                        ),
                        const SizedBox(
                          width: 10.0,
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: CircularPercentIndicator(
                          arcType: ArcType.FULL,
                          restartAnimation: true,
                          radius: 120.0,
                          lineWidth: 5.0,
                          curve: Curves.easeInOutBack,
                          backgroundColor: Colors.transparent,
                          percent: 0.8,
                          animation: true,
                          center: GestureDetector(
                            onTap: () {
                              editPhotoBottomSheet();
                            },
                            child: const CircleAvatar(
                              radius: 50.0,
                              backgroundImage: AssetImage('assets/ledi.jpg'),
                            ),
                          ),
                          progressColor: Colors.amber,
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Center(
                                          child: Text(
                                        'Lediana Kartika Sari',
                                        style: GoogleFonts.openSans(
                                          color: Colors.white,
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Center(
                                child: Text(
                              'Sales Samsung',
                              style: GoogleFonts.openSans(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            )),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              decoration: const BoxDecoration(
                  borderRadius:
                      BorderRadius.only(bottomLeft: Radius.circular(40.0), bottomRight: Radius.circular(40.0)),
                  color: Colors.black),
            )),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 20.0, bottom: 10.0, top: 8.0),
                child: Text(
                  'Sales Report',
                  style: GoogleFonts.openSans(
                    color: Colors.grey,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10.0, right: 25, left: 25),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[100]?.withOpacity(0.7), borderRadius: BorderRadius.circular(40.0)),
                  child: Stack(
                    children: [
                      Positioned(
                        bottom: 30,
                        left: 10,
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 12.0, left: 10.0, bottom: 5.0),
                            child: Text(
                              'Samsung',
                              style: GoogleFonts.openSans(
                                color: Colors.grey,
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SfCircularChart(
                          legend: Legend(
                              padding: 11,
                              itemPadding: 5,
                              isResponsive: true,
                              borderWidth: 1,
                              position: LegendPosition.bottom,
                              isVisible: true),
                          tooltipBehavior: TooltipBehavior(
                              activationMode: ActivationMode.singleTap, enable: true, canShowMarker: true),
                          series: <RadialBarSeries<SalesData, String>>[
                            RadialBarSeries<SalesData, String>(
                                enableTooltip: true,
                                gap: '5',
                                legendIconType: LegendIconType.seriesType,
                                cornerStyle: CornerStyle.bothCurve,
                                dataSource: data,
                                xValueMapper: (SalesData data, _) => data.year,
                                yValueMapper: (SalesData data, _) => data.sales,
                                dataLabelMapper: (SalesData data, _) => data.year,
                                dataLabelSettings: const DataLabelSettings(isVisible: true)),
                          ]),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void editPhotoBottomSheet() {
    Get.bottomSheet(Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0))),
      child: Wrap(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 15.0, bottom: 8.0),
            child: Text(
              'Photo Profile',
              style: GoogleFonts.openSans(
                color: Colors.grey[800],
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Image.asset(
                      'assets/delete.png',
                      width: 50.0,
                      height: 50.0,
                    ),
                    Text(
                      'Delete',
                      style: GoogleFonts.openSans(
                        color: Colors.grey,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  controller.getFilePhotoProfile();
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/galery.png',
                        width: 50.0,
                        height: 50.0,
                      ),
                      Text(
                        'Galery',
                        style: GoogleFonts.openSans(
                          color: Colors.grey,
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    ));
  }
}

class SalesData {
  SalesData(this.year, this.sales);

  final String year;
  final double sales;
}

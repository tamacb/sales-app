import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sales/app/routes/app_pages.dart';
import 'package:sales/app/widgets/loginTextFormFieldBase.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            HexColor('#1D1819'),
            HexColor('#CC9933'),
          ],
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Image.asset(
                'assets/pixel.png',
                width: Get.width * 0.5,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text('Wellcome Promotor',
                  style: GoogleFonts.nunitoSans(
                    color: context.theme.backgroundColor,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: LoginTextFormFieldBase(
                controller: controller,
                hintText: 'email',
                obscureText: false,
                passwordVisibility: false,
                textEditingController: controller.emailEditingController,
              ),
            ),
            const SizedBox(height: 15.0),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: LoginTextFormFieldBase(
                controller: controller,
                hintText: 'password',
                obscureText: false,
                passwordVisibility: false,
                textEditingController: controller.passwordEditingController,
              ),
            ),
            const SizedBox(height: 30.0),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: RoundedLoadingButton(
                width: Get.width * 0.7,
                  key: const Key('buttonLogin'),
                  valueColor: HexColor('#CC9933'),
                  color: Colors.black,
                  controller: controller.btnControllerLogin,
                  onPressed: () async {
                    Get.toNamed(Routes.ADD_DATA);
                    controller.btnControllerLogin.reset();
                  },
                  child: Text(
                    'LOGIN',
                    style: TextStyle(color: HexColor('#CC9933')),
                  )),
            ),
          ],
        ),
      ),
      // floatingActionButton: FloatingActionButton.small(onPressed: () {
      //   Get.changeTheme(Get.isDarkMode ? ThemeData.light() : ThemeData.dark());
      // }),
    );
  }
}

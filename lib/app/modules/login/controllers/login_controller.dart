import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class LoginController extends GetxController {
  //TODO: Implement LoginController

  final emailEditingController = TextEditingController();
  final passwordEditingController = TextEditingController();

  final RoundedLoadingButtonController btnControllerLogin = RoundedLoadingButtonController();


  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:sales/app/routes/app_pages.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

import '../controllers/coba_controller.dart';

class CobaView extends GetView<CobaController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CobaView'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 0),
              child: StreamBuilder<int>(
                stream: controller.stopWatchTimer.rawTime,
                initialData: controller.stopWatchTimer.rawTime.value,
                builder: (context, snap) {
                  final value = snap.data!;
                  final displayTime =
                      StopWatchTimer.getDisplayTime(value, hours: controller.isHours, milliSecond: false);
                  return Text(
                    displayTime,
                    style: const TextStyle(fontSize: 40, fontFamily: 'Helvetica', fontWeight: FontWeight.bold),
                  );
                },
              ),
            ),
          ),
          Container(
            height: 120,
            margin: const EdgeInsets.all(8),
            child: StreamBuilder<List<StopWatchRecord>>(
              stream: controller.stopWatchTimer.records,
              initialData: controller.stopWatchTimer.records.value,
              builder: (context, snap) {
                final value = snap.data!;
                if (value.isEmpty) {
                  return Container();
                }
                return ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemBuilder: (BuildContext context, int index) {
                    final data = value[index];
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8),
                          child: Text(
                            '$index ${data.displayTime}',
                            style: const TextStyle(
                                fontSize: 17,
                                fontFamily: 'Helvetica',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        const Divider(
                          height: 1,
                        )
                      ],
                    );
                  },
                  itemCount: value.length,
                );
              },
            ),
          ),
          GestureDetector(
              onTap: () {
                controller.stopWatchTimer.onExecute.add(StopWatchExecute.start);
              },
              child: const Text('start')),
          GestureDetector(
              onTap: () {
                controller.stopWatchTimer.onExecute.add(StopWatchExecute.stop);
              },
              child: const Text('start')),
          GestureDetector(
              onTap: () {
                controller.stopWatchTimer.setPresetSecondTime(250);
              },
              child: const Text('set by seconds')),
          GestureDetector(
              onTap: () {
                controller.stopWatchTimer.onExecute
                    .add(StopWatchExecute.lap);
              },
              child: const Text('Lap')),
        GestureDetector(
            onTap: (){
              Get.toNamed(Routes.ADD_DATA);
            },
            child: const Text('to another'))
        ],
      ),
    );
  }
}

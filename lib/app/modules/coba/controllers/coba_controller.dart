import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:sales/app/utils/common_utils.dart';
import 'package:sales/app/utils/constaUtils.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

class CobaController extends FullLifeCycleController {
  //TODO: Implement CobaController
  final isHours = true;

  final StopWatchTimer stopWatchTimer = StopWatchTimer(
    mode: StopWatchMode.countUp,
    // onChange: (value) => print('onChange $value'),
    onChangeRawSecond: (value) => logger.i('onChangeRawSecond $value'),
    // onChangeRawMinute: (value) => print('onChangeRawMinute $value'),
  );

  final count = 0.obs;

  final GetStorage _box = GetStorage();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      stopWatchTimer.secondTime.listen((value) {
        _box.write(workDuration, value).whenComplete(() => logger.i('on pause ${_box.read(workDuration)}'));
        _box.write(timerPauseWork, DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now())).whenComplete(() => logger.i('work durations ${_box.read(timerPauseWork)}'));
        logger.i('detik saiki $value , ${DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now())}');
      });
    }
  }

  @override
  void onInit() {
    super.onInit();
    WidgetsBinding.instance!.addObserver(this);
    logger.i('on pause ${_box.read(workDuration)}');
    initWhenWorkingOn();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    WidgetsBinding.instance!.removeObserver(this);
  }

  initWhenWorkingOn(){
    var onWorkDuration = _box.read(workDuration);
    if (onWorkDuration != null){
      var pausedTimer = _box.read(timerPauseWork);
      DateTime dateTimePaused = DateTime.parse(pausedTimer);
      DateTime dateTimeNow = DateTime.parse(DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()));
      Duration workingHours = dateTimeNow.difference(dateTimePaused);
      logger.i('detik saiki on resumed ${workingHours.inSeconds}');
      logger.i('current work duration ${_box.read(workDuration)}');
      var totalWorkingTime = onWorkDuration + workingHours.inSeconds;
      logger.i('total $totalWorkingTime');
      stopWatchTimer.setPresetSecondTime(totalWorkingTime);
      stopWatchTimer.onExecute.add(StopWatchExecute.start);
    }
  }

}

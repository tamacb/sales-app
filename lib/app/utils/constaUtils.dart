import 'package:get_storage/get_storage.dart';


//Container Storage
const containerUserStorage = 'UserStorage';
const containerTimerStorage = 'TimerStorage';

//Key Storage User
const workDuration = 'duration';
const timerStartWork = 'timerStart';
const timerEndWork = 'timerEnd';
const timerPauseWork = 'timerPauseEnd';
const isStartedWork = 'isStartedWork';

//Key LifeCycle
const onDetachedDevice = 'onDetachedDevice';
